/*
 * Leg.cpp
 * Copyright (C) 2021 Adam Stewart <adam.stewart@paritymedical.com>
 *
 */

#include <cstdlib>

#include "Leg.h"
#include "Globals.h"

Leg::Leg(uint16_t baseHeightMM, uint16_t throwMM, uint16_t totalPulses, uint16_t maxHeightMM, uint16_t minHeightMM) :
    mBaseHeightMM(baseHeightMM), mThrowMM(throwMM), mTotalPulses(totalPulses), mMaxHeightMM(maxHeightMM), mMinHeightMM(minHeightMM)
{
    reset();
}

void Leg::reset(void) {
    mStatus.upOn = PORT->Group[0].OUT.reg & PORT_PA07 ? false : true;
    mStatus.downOn = PORT->Group[0].OUT.reg & PORT_PA11 ? false : true;
    mStatus.goingUp = false;
    mStatus.goingDown = false;
    mStatus.atTheTop = false;
    mStatus.atTheBottom = false;
    mStatus.grayMatchCutoff = false;
    mStatus.overCurrentCutoff = false;
    mStatus.runtimeCutoff = false;
    mStatus.autoMode = false;
    mStatus.calibrating = false;

    mGrayCode = makeGrayCode();
    mGrayCodeMatchTimer = 0;
    mGrayCodeMatchValue = (TL6_GRAY_MATCH_TIMEOUT * 1000) / TL6_TIMER_INTERVAL;

    mCoolDownTimeout = 0;
    mCoolDownCounter = 0;

    mRuntimeTimeout = (TL6_MAX_RUNTIME * 1000) / TL6_TIMER_INTERVAL;
    mRuntimeCounter = 0;

    mBatteryCurrentAtStart = 0;

    mUpPulses = 0;
    mDownPulses = 0;
    mHeightPulses = 0;
    mHeightMM = 0;
    mPulsePerMM = 0;

    mTotalHeightMM = mBaseHeightMM + mThrowMM;
    mPulsePerMM = (float)mTotalPulses / (float)mThrowMM;

    mMaxHeightPulses = (mMaxHeightMM - mBaseHeightMM) * mPulsePerMM;
    mMinHeightPulses = (mMinHeightMM - mBaseHeightMM) * mPulsePerMM;
}

void Leg::upOff(void) {
    // NOTE: On/Off are REVERSED from the 24v version. 1 = off, 0 = on.
    PORT->Group[0].OUT.reg |= PORT_PA07;
    mStatus.upOn = false;
    mBatteryCurrentAtStart = 0.0;
    mStatus.goingUp = false;
    mStatus.goingDown = false;
}

void Leg::downOff(void) {
    // NOTE: On/Off are REVERSED from the 24v version. 1 = off, 0 = on.
    PORT->Group[0].OUT.reg |= PORT_PA11;
    mStatus.downOn = false;
    mBatteryCurrentAtStart = 0.0;
    mStatus.goingUp = false;
    mStatus.goingDown = false;
}

uint8_t Leg::makeGrayCode(void) {
    uint8_t ret = 0;

    if(PORT->Group[0].IN.reg & PORT_PA06) {
        ret |= (1 << 1);
    }

    if(PORT->Group[0].IN.reg & PORT_PA05) {
        ret |= (1 << 0);
    }

    return ret;
}

void Leg::startCooldown(const uint32_t timer) {
    mCoolDownTimeout = timer / TL6_TIMER_INTERVAL;
    mStatus.coolDown = true;
}

void Leg::up(void) {
    if(mStatus.atTheTop) {
        upOff();
        return;
    }
    
    if(mStatus.coolDown) {
        upOff();
        return;
    }

    if(mStatus.downOn) {
        downOff();
        delay(5);
    }

    // TODO: Check this!!! Why do I have the - TL6_MAX_CURRENT.
    // if(screenData.current) {
    //     mBatteryCurrentAtStart = std::abs(screenData.current - TL6_MAX_CURRENT);
    // } else {
    //     mBatteryCurrentAtStart = std::abs(screenData.current);
    // }
    mBatteryCurrentAtStart = screenData.current;

    // NOTE: On/Off are REVERSED from the 24v version. 1 = off, 0 = on.
    PORT->Group[0].OUT.reg &= ~PORT_PA07;

    mStatus.upOn = true;
    mStatus.overCurrentCutoff = false;
    mStatus.runtimeCutoff = false;
    mStatus.grayMatchCutoff = false;
    mStatus.atTheBottom = false;

    mRuntimeCounter = 0;
    mUpPulses = 0;
}

void Leg::down(void) {
    if(!mStatus.calibrating && mStatus.atTheBottom) {
        downOff();
        return;
    }

    if(mStatus.coolDown) {
         downOff();
        return;
    }

    if(mStatus.upOn) {
        upOff();
        delay(5);
    }

    // TODO: Check this!!! Why do I have the - TL6_MAX_CURRENT.
    // if(screenData.current) {
    //     mBatteryCurrentAtStart = std::abs(screenData.current - TL6_MAX_CURRENT);
    // } else {
    //     mBatteryCurrentAtStart = std::abs(screenData.current);
    // }
    mBatteryCurrentAtStart = screenData.current;

    // NOTE: On/Off are REVERSED from the 24v version. 1 = off, 0 = on.
    PORT->Group[0].OUT.reg &= ~PORT_PA11;

    mStatus.downOn = true;
    mStatus.overCurrentCutoff = false;
    mStatus.runtimeCutoff = false;
    mStatus.grayMatchCutoff = false;
    mStatus.atTheTop = false;

    mRuntimeCounter = 0;
    mDownPulses = 0;
}

void Leg::stop(void) {
    upOff();
    downOff();
    // startCooldown(TL6_END_STOP_COOLDOWN * 1000);
}

void Leg::cutoffCheck(void) {
    if(mStatus.upOn && mHeightMM >= TL6_DEFAULT_MAX_HEIGHT_MM) {
        mStatus.atTheTop = true;
        mStatus.atTheBottom = false;
    }

    if(mStatus.downOn && (mHeightMM <= TL6_DEFAULT_MIN_HEIGHT_MM)) {
        mStatus.atTheBottom = true;
        mStatus.atTheTop = false;
    }

    if(mStatus.calibrating) {
        if(mStatus.overCurrentCutoff || mStatus.grayMatchCutoff) {
            mStatus.goingUp = false;
            mStatus.goingDown = false;
            mStatus.autoMode = false;
            mStatus.calibrating = false;
            mRuntimeCounter = 0;
            mUpPulses = 0;
            mDownPulses = 0;
            mHeightPulses = 0;
            mHeightMM = 0;
            mStatus.atTheTop = false;
            mStatus.atTheBottom = true;
        } else {
            calibrate();
        }
    }
}

void Leg::service(void) {
    mStatus.upOn = PORT->Group[0].OUT.reg & PORT_PA07 ? false : true;
    mStatus.downOn = PORT->Group[0].OUT.reg & PORT_PA11 ? false : true;

    if(!mStatus.upOn) {
        mStatus.goingUp = false;
    }

    if(!mStatus.downOn) {
        mStatus.goingDown = false;
    }

    if(mStatus.upOn || mStatus.downOn) {

        if(++mRuntimeCounter >= mRuntimeTimeout) {
            mStatus.goingDown = false;
            mStatus.goingUp = false;
            mStatus.runtimeCutoff = true; 
            stop();
        }

        mLegCurrentUsage = mBatteryCurrentAtStart - screenData.current;
        // if(std::abs(screenData.current) >= (mBatteryCurrentAtStart + TL6_MAX_CURRENT)) {
        if(mLegCurrentUsage > TL6_MAX_CURRENT) {
            mStatus.overCurrentCutoff = true;
            cutoffCheck();
            stop();
        } 
    }

    uint8_t tmpCode = makeGrayCode();
    if(mGrayCode != tmpCode) {
        if((mGrayCode == 3 && tmpCode == 1) || (mGrayCode == 0 && tmpCode == 2)) {
        // if(mGrayCode == 3 && tmpCode == 1) {
            mStatus.goingDown = true;
            mStatus.goingUp = false;
            ++mDownPulses;
            --mHeightPulses;
        } else if((mGrayCode == 2 && tmpCode == 0) || (mGrayCode == 1 && tmpCode == 3)) {
        // } else if(mGrayCode == 2 && tmpCode == 0) {
            mStatus.goingUp = true;
            mStatus.goingDown = false;
            ++mUpPulses;
            ++mHeightPulses;
        }

        if(mHeightPulses < 0) {
            mHeightPulses = 0;
        }

        mGrayCode = tmpCode;

        mGrayCodeMatchTimer = 0;
    } else {
        if(mStatus.upOn || mStatus.downOn) {
            if(++mGrayCodeMatchTimer >= mGrayCodeMatchValue) {
                mGrayCodeMatchTimer = 0;
                mStatus.grayMatchCutoff = true;
                cutoffCheck();
                stop();
            }
        }

    }

    if(mStatus.coolDown) {
        if(++mCoolDownCounter >= mCoolDownTimeout) {
            mStatus.coolDown = false;
            mCoolDownCounter = 0;
            mCoolDownTimeout = 0;
        }        
    }

    if(mStatus.autoMode) {
        if(mHeightPulses >= (mDesiredHeightPulses - 5) && mHeightPulses <= (mDesiredHeightPulses + 5)) {
            stop();
            mStatus.autoMode = false;
            mStatus.goingDown = false;
            mStatus.goingUp = false;
        }
    }

    if(mStatus.upOn && mHeightPulses >= mMaxHeightPulses) {
        mStatus.goingDown = false;
        mStatus.goingUp = false;
        mStatus.atTheTop = true;
        mStatus.atTheBottom = false;
        stop();
    } 
    
    if(!mStatus.calibrating && mStatus.downOn && mHeightPulses <= mMinHeightPulses) {
        mStatus.goingDown = false;
        mStatus.goingUp = false;
        mStatus.atTheTop = false;
        mStatus.atTheBottom = true;
        stop();
    }

}

void Leg::goToHeightPulses(const uint16_t height) {
    mDesiredHeightPulses = height;
    mStatus.autoMode = true;
    if(mDesiredHeightPulses > mHeightPulses) {
        up();
    } else {
        down();
    }
}

void Leg::goToHeightMM(uint16_t height) {
    if(height < mBaseHeightMM) {
        height = mBaseHeightMM;
    }

    float pulses = (float)(height - mBaseHeightMM) * mPulsePerMM;

    if((uint16_t)pulses > mTotalPulses) {
        pulses = mTotalPulses - 5;
    }

    goToHeightPulses((uint16_t)pulses);
}

void Leg::calibrate(void) {
    mStatus.calibrating = true;
    down();
}

void Leg::outputDiagnosticInfo(const std::string& msg) {
    outputDiagnosticInfo();
    Serial.print("\n\rMessage: ");
    Serial.println(msg.c_str());
}

void Leg::outputDiagnosticInfo(void) {
    char outputBuf[150];
    bool hall1 = PORT->Group[0].IN.reg & PORT_PA06;
    bool hall2 = PORT->Group[0].IN.reg & PORT_PA05;

    sprintf(outputBuf, "%c[J%c[HUP: %s\n\rDOWN: %s\n\r", 0x1B, 0x1B, mStatus.upOn ? "ON ": "OFF", mStatus.downOn ? "ON " : "OFF");
    Serial.println(outputBuf);
    sprintf(outputBuf, "Hall 1: %d\n\rHall 2: %d\n\r", hall1 ? 1 : 0, hall2 ? 1 : 0);
    Serial.println(outputBuf);


    Serial.print("Gray Code Match Timer: ");
    Serial.println(mGrayCodeMatchTimer);
    Serial.print("Gray Code Match Value: ");
    Serial.println(mGrayCodeMatchValue);

    Serial.print("\n\rCurrent At Start: ");
    Serial.println(mBatteryCurrentAtStart);
    Serial.print("Leg Current Usage: ");
    Serial.println(mLegCurrentUsage);
    Serial.print("Current Cutoff:   ");
    // Serial.println(mBatteryCurrentAtStart + TL6_MAX_CURRENT);
    Serial.println(TL6_MAX_CURRENT);

    Serial.print("\n\rGray Match Cutoff:   ");
    Serial.println(mStatus.grayMatchCutoff ? "TRUE " : "FALSE");
    Serial.print("Over current Cutoff: ");
    Serial.println(mStatus.overCurrentCutoff ? "TRUE " : "FALSE");
    Serial.print("Runtime Cutoff:      ");
    Serial.println(mStatus.runtimeCutoff ? "TRUE " : "FALSE");

    Serial.print("\n\rAt the TOP:    ");
    Serial.println(mStatus.atTheTop ? "TRUE " : "FALSE");
    Serial.print("At the BOTTOM: ");
    Serial.println(mStatus.atTheBottom ? "TRUE " : "FALSE");

    Serial.print("\n\rUp Pulses:    ");
    Serial.println(mUpPulses);
    Serial.print("Down Pulses:  ");
    Serial.println(mDownPulses);
    Serial.print("Height:       ");
    Serial.println(mHeightPulses);
    Serial.print("Total Pulses: ");
    Serial.println(mTotalPulses);

    Serial.print("\n\rRuntime counter: ");
    Serial.println(mRuntimeCounter);
    Serial.print("Runtime timeout: ");
    Serial.println(mRuntimeTimeout);

    Serial.print("\nTotal Height MM:       ");
    Serial.println(mTotalHeightMM);
    Serial.print("Pulses Per MM:         ");
    Serial.println(mPulsePerMM);
    Serial.print("Current height MM:     ");
    Serial.println((mHeightPulses / mPulsePerMM) + mBaseHeightMM);
    Serial.print("Desired Height Pulses: ");
    Serial.println(mDesiredHeightPulses);

    Serial.print("\nCalibrating:      ");
    Serial.println(mStatus.calibrating);

    Serial.print("\nCooldown Timeout: ");
    Serial.println(mCoolDownTimeout);
    Serial.print("Cooldown Counter: ");
    Serial.println(mCoolDownCounter);
    Serial.print("Cooling Down:     ");
    Serial.println(mStatus.coolDown);

    Serial.print("\nMax Height Pulses: ");
    Serial.println(mMaxHeightPulses);
    Serial.print("Max Height MM:     ");
    Serial.println(mMaxHeightMM);
    Serial.print("\nMin Height Pulses: ");
    Serial.println(mMinHeightPulses);
    Serial.print("Min Height MM:     ");
    Serial.println(mMinHeightMM);

    Serial.print("\nBattery Current: ");
    Serial.println(screenData.current);

    if(mStatus.goingUp) {
        Serial.println("GOING UP    ");
    } else if(mStatus.goingDown) {
        Serial.println("GOING DOWN  ");
    } else {
        Serial.println("            ");
    }

    Serial.println("Commands: UP: a, DOWN: b, STOP: c, RESET: d, CALIBRATE: e");

}
