/*
 * ui.cpp
 * Copyright (C) 2020 Adam Stewart <adam.stewart@paritymedical.com>
 *
 */

#include "ui.h"
#include "Main.h"
#include "Globals.h"

void initUI(void) {
}

void trigger0() { btnUp_press(); }
void trigger1() { btnUp_release(); }
void trigger2() { btnDown_press(); }
void trigger3() { btnDown_release(); }
void trigger4() { btnSit_release(); }
void trigger5() { btnStand_release(); }
void trigger6() { btnLightOff_release(); }
void trigger7() { btnPower_release(); }
void trigger8() { btnPowerConfirm_release(); }
void trigger9() { btnOverVoltage_release(); }
void trigger10() { btnOverload_release(); }
void trigger11() { btnLights_25(); };
void trigger12() { btnLights_50(); };
void trigger13() { btnLights_75(); };
void trigger14() { btnLights_100(); };
void trigger15() { btnOverheat_release(); }

void btnUp_release() {
    if(!legs.status().calibrating) {
        legs.upOff();
    }
}

void btnUp_press() {
    if(screenData.state != STATE_EMPTY && screenData.state != STATE_NONE && !flags.moving && !flags.overload && !flags.overVoltage) {
        if(!legs.status().calibrating) {
            legs.up();
        }
    }
}

void btnDown_release() {
    if(!legs.status().calibrating && !flags.overload && !flags.overVoltage) {
        legs.downOff();
    }
}

void btnDown_press() {
    if(screenData.state != STATE_EMPTY && screenData.state != STATE_NONE && !flags.moving && !flags.overload && !flags.overVoltage) {
        if(!legs.status().calibrating) {
            legs.down();
        }
    }
}

void btnSit_release() {
    if(screenData.state != STATE_EMPTY && screenData.state != STATE_NONE && !flags.moving && !flags.overload && !flags.overVoltage) {
        if(!legs.status().calibrating) {
            legs.goToHeightMM(TL6_SIT_HEIGHT_MM);
        }
    }
}

void btnStand_release() {
    if(screenData.state != STATE_EMPTY && screenData.state != STATE_NONE && !flags.moving && !flags.overload && !flags.overVoltage) {
        if(!legs.status().calibrating) {
            legs.goToHeightMM(TL6_STAND_HEIGHT_MM);
        }
    }
}

void btnLightOff_release() {
    setLED1Brightness(0);
}

void btnLights_25() {
    setLED1Brightness(LED_MAX_BRIGHTNESS / 4);
}

void btnLights_50() {
    setLED1Brightness(LED_MAX_BRIGHTNESS / 2);
}

void btnLights_75() {
    setLED1Brightness(LED_MAX_BRIGHTNESS * 0.75);
}

void btnLights_100() {
    setLED1Brightness(LED_MAX_BRIGHTNESS);
}

void btnPower_release() {
    if(!legs.status().upOn && !legs.status().downOn) {
        if(outgoingQueue.size() < HMI_MESSAGE_BUFFER_SIZE) {
            outgoingQueue.push_back(HMI_LOAD_ON);

            loadCheckTimer = 0;
            flags.loadCheck = 1;

            runtimeAccumulator = 0;
            runtimeAvgCount = 0;
        }
    }
}

void btnPowerConfirm_release() {
    if(!legs.status().upOn && !legs.status().downOn) {
        if(outgoingQueue.size() < HMI_MESSAGE_BUFFER_SIZE) {
            outgoingQueue.push_back(HMI_LOAD_OFF);
            nextion.writeStr("page Main");
            currentPage = PAGE_MAIN;

            loadCheckTimer = 0;
            flags.loadCheck = 1;
        }
    }

    // NOTE: Force an update of the runtime.
    nextion.writeStr("runtime.txt", "");
}

void btnOverload_release() {
    flags.overload = 0;
    if(outgoingQueue.size() < HMI_MESSAGE_BUFFER_SIZE) {
        outgoingQueue.push_back(HMI_OVERLOAD_ACK);
    }
}

void btnOverVoltage_release() {
    flags.overVoltage = 0;
    if(outgoingQueue.size() < HMI_MESSAGE_BUFFER_SIZE) {
        outgoingQueue.push_back(HMI_OVERVOLTAGE_ACK);
    }
}

void btnOverheat_release() {
    flags.cooledOff = 0;
    if(outgoingQueue.size() < HMI_MESSAGE_BUFFER_SIZE) {
        outgoingQueue.push_back(HMI_COOLEDOFF_ACK);
    }
}
