/*
 * DST.h
 * Copyright (C) 2021 Adam Stewart <adam.stewart@paritymedical.com>
 *
 * Distributed under terms of the MIT license.
 */

#ifndef DST_H
#define DST_H

#include <map>

struct DSTDates {
    int springForward;
    int fallBack;    
};

extern bool dstAdjusted;

extern std::map<int, DSTDates> dstDates;

#endif /* !DST_H */
