#ifndef UI_H
#define UI_H

#include "Globals.h"
#include "Main.h"

void initUI(void);

// void trigger1();
// void trigger2();
// void trigger3();
// void trigger4();
// void trigger5();
// void trigger6();
// void trigger7();
// void trigger8();
// void trigger9();
// void trigger10();
// void trigger11();
// void trigger12();
// void trigger13();
// void trigger14();
// void trigger15();

void btnUp_release();
void btnUp_press();
void btnDown_release();
void btnDown_press();
void btnSit_release();
void btnStand_release();
void btnLightOff_release();
void btnPower_release();
void btnPowerConfirm_release();
void btnOverVoltage_release();
void btnOverload_release();
void btnOverheat_release();
void btnLights_25();
void btnLights_50();
void btnLights_75();
void btnLights_100();

#endif /* !UI_H */
