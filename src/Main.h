#ifndef MAIN_H
#define MAIN_H

#include <string>
#include <vector>

void onRequestHandler(void);
void handleSerialInput(const char in);

void processMessageFromQueue(void);
void updateScreen(void);

void setLED1Brightness(uint8_t value);
void setLED2Brightness(uint8_t value);

std::vector<std::string> split(std::string s, std::string delim);
void executeCmd(void);

void showCmdMenu(void);
void dstCheck(void);
#endif /* !MAIN_H */
