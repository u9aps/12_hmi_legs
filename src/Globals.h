#ifndef GLOBALS_H
#define GLOBALS_H

#include <list>
#include <string>
#include <vector>
#include "EasyNextionLibrary.h"
#include "Shared.h"
#include "Leg.h"
#include <RTCZero.h>

#define MAX_MESSAGE_LIST_SIZE 10
#define RUNTIME_NUM_TO_AVERAGE  20      // NOTE: The timer is half a second.

#define LED1    A1
#define LED2    A2
#define LED_MAX_BRIGHTNESS      255

#define PAGE_MAIN                   1           // NOTE: These are Nextion page numbers that correspond with the UI design.
#define PAGE_POWER_OFF              2
#define PAGE_CLEAN_START            3
#define PAGE_CLEAN_END              12
#define PAGE_OVERVOLTAGE            13
#define PAGE_OVERLOAD               14
#define PAGE_OVERHEAT               15
#define PAGE_BATTERY_LOW            16
#define PAGE_COOLED_OFF             17

#define MARCH       3
#define OCTOBER     10

#define CMD_MODE_TIMEOUT_VALUE      30      // NOTE: Seconds.

#define LOAD_CHECK_TIMEOUT          2       // NOTE: Seconds. How long to wait after a button press to confirm the load went off.

struct  Flags_t {
    uint16_t updateScreen : 1;
    uint16_t overload : 1;
    uint16_t overVoltage : 1;
    uint16_t batteryOverheat : 1;
    uint16_t cooledOff : 1;
    uint16_t moving : 1;
    uint16_t checkScreenSleep: 1;
    uint16_t screenIsAsleep: 1;
    uint16_t loadCheck: 1;
}__attribute((packed));

struct ScreenData_t {
    uint8_t soc;
    uint16_t runtime;
    bool    dsg;
    bool    loadStatus;
    BatteryStates_e state;
    float   current;
}__attribute((packed));

extern RTCZero rtc;

// enum NextionPages_e : uint8_t { PAGE_MAIN, PAGE_BATTERY_LOW, PAGE_OVERLOAD, PAGE_OVERHEAT, PAGE_OVERVOLTAGE, PAGE_COOLED_OFF };
// extern NextionPages_e currentPage;
extern int currentPage;
extern EasyNex nextion;
extern Leg legs;

extern std::vector<uint8_t> i2cRecvBuf;

extern std::list<hmiControllerMessage> incomingQueue;
extern std::list<hmiControllerMessageCodes_e> outgoingQueue;

extern Flags_t flags;
extern ScreenData_t screenData;

extern long runtimeAccumulator;
extern uint8_t runtimeAvgCount;
extern std::string runtime;

extern uint8_t led1CurrentBrightness;
extern uint8_t led2CurrentBrightness;

extern std::string extraMsg;
extern std::string cmd;
extern bool cmdMode;
extern bool showLegsData;
extern uint8_t cmdModeTimeout;

extern uint8_t loadCheckTimer;

#endif /* !GLOBALS_H */
