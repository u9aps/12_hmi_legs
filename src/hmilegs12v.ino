#include <vector>
#include <cstdint>
#include <cstring>

#include <Wire.h>
#include <Adafruit_SleepyDog.h>
#include "Main.h"
#include "Timers.h"
#include "Shared.h"
#include "Globals.h"
#include "DST.h"

char outputBuf[256] = {};

void setup() {
    // NOTE: Outputs.
    PORT->Group[0].DIR.reg |= PORT_PA11;        // NOTE: Leg down
    PORT->Group[0].OUT.reg &= ~PORT_PA11;
    PORT->Group[0].DIR.reg |= PORT_PA07;        // NOTE: Leg up
    PORT->Group[0].OUT.reg &= ~PORT_PA07;

    PORT->Group[0].OUT.reg |= PORT_PA07;
    PORT->Group[0].OUT.reg |= PORT_PA11;

    PORT->Group[0].OUT.reg |= PORT_PA07;
    PORT->Group[0].OUT.reg |= PORT_PA11;

    Serial.begin(115200);
    nextion.begin(57600);

    Wire.begin(HMI_BOARD_SLA);
    Wire.onRequest(onRequestHandler);
    Wire.onReceive(onReceiveHandler);


    // NOTE: Inputs.
    PORT->Group[0].PINCFG[6].reg |= (PORT_PINCFG_INEN | PORT_PINCFG_PULLEN);    // NOTE: Hall 1.
    PORT->Group[0].PINCFG[5].reg |= (PORT_PINCFG_INEN | PORT_PINCFG_PULLEN);    // NOTE: Hall 2.

    // TCC0_Init();
    TC3_Init();
    TC4_Init();
    TC5_Init();

    asm(".global _printf_float"); // NOTE: Enable floating point output in printf. Uses a LOT of memory.

    Watchdog.enable(3000);

    rtc.begin();

    int tempHours = nextion.readNumber("rtc3");
    int tempMinutes = nextion.readNumber("rtc4");
    int tempSeconds = nextion.readNumber("rtc5");
    int tempDay = nextion.readNumber("rtc2");
    int tempMonth = nextion.readNumber("rtc1");
    int tempYear = nextion.readNumber("rtc0") - 2000;
    int tempDST = nextion.readNumber("Splash.DSTValue.val");

    rtc.setHours(tempHours);
    rtc.setMinutes(tempMinutes);
    rtc.setSeconds(tempSeconds);
    rtc.setDay(tempDay);
    rtc.setMonth(tempMonth);
    rtc.setYear(tempYear);
    dstAdjusted = tempDST == 1 ? true : false;

    Serial.println("Init done.");
}

void loop() {
    nextion.NextionListen();

    processMessageFromQueue();

    if(flags.checkScreenSleep) {
        dstCheck();
        flags.screenIsAsleep = (nextion.readNumber("sleep") == 1) ? 1 : 0;
        currentPage = nextion.readNumber("dp");
        flags.checkScreenSleep = 0;
    }

    if(flags.updateScreen) {

        updateScreen();

        if(cmdMode) {
            if(cmdModeTimeout >= CMD_MODE_TIMEOUT_VALUE) {
                cmdMode = false;
                cmdModeTimeout = 0;
            } else {
                showCmdMenu();
            }
        }

        if(showLegsData) {
            legs.outputDiagnosticInfo(extraMsg);
        }

        flags.updateScreen = 0;
    }


    nextion.NextionListen();

    if(Serial.available()) {
        char in = Serial.read();
        handleSerialInput(in);
    }

    Watchdog.reset();
}

void onRequestHandler(void) {
    uint8_t queueSize = outgoingQueue.size();
    Wire.write(queueSize);
    if(queueSize > 0) {
        for(hmiControllerMessageCodes_e& code : outgoingQueue) {
            Wire.write(code);
        }
    }
    outgoingQueue.clear();
}

void onReceiveHandler(int len) {
    if(len != sizeof(hmiControllerMessage)) {
        return;        
    }

    hmiControllerMessage incoming;
    i2cRecvBuf.clear();

    while(Wire.available()) {
        uint8_t c = Wire.read();
        i2cRecvBuf.push_back(c);
    }
    
    std::memcpy(&incoming, i2cRecvBuf.data(), sizeof(hmiControllerMessage));
    incomingQueue.push_back(incoming);
}

void processMessageFromQueue(void) {
    if(incomingQueue.size() <= 0) {
        return;
    }

    hmiControllerMessage msg = incomingQueue.front();
    incomingQueue.pop_front();

    switch(msg.code) {
        case HMI_STOP: { } break; // NOTE: The HMI board will never receive this message.
        case HMI_LOAD_ON: { } break; // NOTE: The HMI board will never receive this message. 
        case HMI_LOAD_OFF: { } break; // NOTE: The HMI board will never receive this message. 
        case HMI_IAH: { } break; // NOTE: The HMI board will never receive this message. 

        case HMI_DATA: { 
            hmiControllerDataPacket pkt = {};
            std::memcpy(&pkt, msg.payload, sizeof(hmiControllerDataPacket));

            screenData.soc = pkt.soc;
            screenData.runtime = pkt.runtime;
            screenData.dsg = pkt.status & 0x01;
            screenData.loadStatus = pkt.status & 0x02;

            uint8_t tmp = pkt.stateAndErrors & 0x00FF; 
            screenData.state = static_cast<BatteryStates_e>(tmp);

            tmp = (pkt.stateAndErrors & 0xFF00) >> 8;
            flags.overload = (tmp & 0x01) ? 1 : 0;
            flags.overVoltage = (tmp & 0x02) ? 1 : 0;
            flags.batteryOverheat = (tmp & 0x04) ? 1 : 0;
            flags.cooledOff = (tmp & 0x08) ? 1 : 0;
            flags.moving = (tmp & 0x10) ? 1 : 0;

            screenData.current = (float)pkt.current / 1000.0f;
        } break;


        case HMI_AYT: { 
            Serial.println("Is anybody out there.");
        } break; 


        case HMI_BAD_MESSAGE: { 
            Serial.println("Bad message.");
        } break;

        default: {
            Serial.println("Unknown mesage code.");
        } break;
    }

}

void setLED1Brightness(uint8_t value) {
    analogWrite(LED1, value);
}

void setLED2Brightness(uint8_t value) {
    analogWrite(LED2, value);
}

void updateScreen(void) {
    int hours = 0;
    int minutes = 0;

    if(flags.moving && flags.screenIsAsleep) {
        nextion.writeNum("sleep", 0);
    }

    // NOTE: We don't display the runtime when charing as it's wildy inaccurate.
    if(screenData.dsg) {
        runtimeAccumulator += screenData.runtime;
        ++runtimeAvgCount;
    } 

    if(runtimeAvgCount >= RUNTIME_NUM_TO_AVERAGE) {
        hours = (runtimeAccumulator / RUNTIME_NUM_TO_AVERAGE) / 60;
        minutes = (runtimeAccumulator / RUNTIME_NUM_TO_AVERAGE) % 60;
        runtimeAccumulator = 0;
        runtimeAvgCount = 0;

        if(hours < 24) {
            runtime = std::to_string(hours) + "h " + std::to_string(minutes) + "m";
        } else {
            runtime = "";
        }
    }

    if((screenData.state != STATE_CHARGED) && (screenData.state != STATE_CHARGING) && (screenData.state != STATE_EMPTY) && (screenData.loadStatus)) {
        nextion.writeStr("runtime.txt", runtime.c_str());
    } else {
        nextion.writeStr("runtime.txt", "");
    }

    std::string socStr = std::to_string(screenData.soc);
    socStr += "%";
    nextion.writeStr("soc.txt", socStr.c_str());

    if(loadCheckTimer >= LOAD_CHECK_TIMEOUT) {
        nextion.writeNum("Main.btnPower.picc", screenData.loadStatus ? 1 : 0); 
        flags.loadCheck = 0;
    }
    // NOTE: The PICC value for the low power screen button is 50 as it has the rounded corners shaded.
    nextion.writeNum("BatteryLow.btnLowPower.picc", screenData.loadStatus ? 1 : 50); 

    if(screenData.state == STATE_CHARGED) {
        nextion.writeNum("soc.picc", 38);
    } else if(screenData.state == STATE_CHARGING) {
        nextion.writeNum("soc.picc", 39);
    } else if(screenData.state == STATE_EMPTY) {
        nextion.writeNum("soc.picc", 0);
    } else {
        if(screenData.soc < 6) {
            nextion.writeNum("soc.picc", 0);
        } else if((screenData.soc >= 6) && (screenData.soc < 13)) {
            nextion.writeNum("soc.picc", 23);
        } else if((screenData.soc >= 13) && (screenData.soc < 19)) {
            nextion.writeNum("soc.picc", 24);
        } else if((screenData.soc >= 19) && (screenData.soc < 25)) {
            nextion.writeNum("soc.picc", 25);
        } else if((screenData.soc >= 25) && (screenData.soc < 31)) {
            nextion.writeNum("soc.picc", 26);
        } else if((screenData.soc >= 31) && (screenData.soc < 38)) {
            nextion.writeNum("soc.picc", 27);
        } else if((screenData.soc >= 38) && (screenData.soc < 44)) {
            nextion.writeNum("soc.picc", 28);
        } else if((screenData.soc >= 44) && (screenData.soc < 50)) {
            nextion.writeNum("soc.picc", 29);
        } else if((screenData.soc >= 50) && (screenData.soc < 56)) {
            nextion.writeNum("soc.picc", 30);
        } else if((screenData.soc >= 56) && (screenData.soc < 63)) {
            nextion.writeNum("soc.picc", 31);
        } else if((screenData.soc >= 63) && (screenData.soc < 69)) {
            nextion.writeNum("soc.picc", 32);
        } else if((screenData.soc >= 69) && (screenData.soc < 75)) {
            nextion.writeNum("soc.picc", 33);
        } else if((screenData.soc >= 75) && (screenData.soc < 81)) {
            nextion.writeNum("soc.picc", 34);
        } else if((screenData.soc >= 81) && (screenData.soc < 88)) {
            nextion.writeNum("soc.picc", 35);
        } else if((screenData.soc >= 88) && (screenData.soc < 94)) {
            nextion.writeNum("soc.picc", 36);
        } else if((screenData.soc >= 94) && (screenData.soc < 100)) {
            nextion.writeNum("soc.picc", 37);
        } else if(screenData.soc >= 100) {
            nextion.writeNum("soc.picc", 38);
        }
    }

    // currentPage = nextion.currentPageId;

    if(flags.overload) {
        if(currentPage != PAGE_OVERLOAD) {
            nextion.writeStr("page Overload");
            currentPage = PAGE_OVERLOAD;
        }
    } else if(flags.batteryOverheat) {
        if(currentPage != PAGE_OVERHEAT) {
            nextion.writeStr("page Overheat");
            currentPage = PAGE_OVERHEAT;
        }
    } else if(flags.overVoltage) {
        if(currentPage != PAGE_OVERVOLTAGE) {
            nextion.writeStr("page OverVoltage");
            currentPage = PAGE_OVERVOLTAGE;
        }
    } else if(flags.cooledOff) {
        if(currentPage != PAGE_COOLED_OFF) {
            nextion.writeStr("page CooledOff");
            currentPage = PAGE_COOLED_OFF;
        }
    } else if(screenData.state == STATE_VERY_LOW || screenData.state == STATE_EMPTY) {
        if(currentPage != PAGE_BATTERY_LOW && currentPage != PAGE_POWER_OFF) { 
            nextion.writeStr("page BatteryLow");
            currentPage = PAGE_BATTERY_LOW;
        }
    } else {
        if(currentPage != PAGE_MAIN && !(currentPage >= 3 && currentPage <= 12) && currentPage != PAGE_POWER_OFF) {
            nextion.writeStr("page Main");
            currentPage = PAGE_MAIN;
        }
    }
}

void dstCheck(void) {
    int month = rtc.getMonth();
    int hours = rtc.getHours();

    int sf = dstDates[rtc.getYear()].springForward;
    int fb = dstDates[rtc.getYear()].fallBack;

    if(month == MARCH && rtc.getDay() == sf) {
        if(!dstAdjusted && (rtc.getHours() >= 1)) {
            nextion.writeNum("sleep", 0);
            nextion.writeNum("rtc3", hours + 1);
            nextion.writeNum("Splash.DSTValue.val", 1);
            nextion.writeNum("Main.updateDST.val", 1);
            rtc.setHours(hours + 1);
            dstAdjusted = true;
        }
    } else if(month == OCTOBER && rtc.getDay() == fb) {
        if(!dstAdjusted && (rtc.getHours() >= 2)) {
            nextion.writeNum("sleep", 0);
            nextion.writeNum("rtc3", hours - 1);
            nextion.writeNum("Splash.DSTValue.val", 1);
            nextion.writeNum("Main.updateDST.val", 1);
            rtc.setHours(hours - 1);
            dstAdjusted = true;
        }
    } else {
        if(dstAdjusted) {
            nextion.writeNum("Splash.DSTValue.val", 0);
            nextion.writeNum("Main.updateDST.val", 1);
            Serial.println("Unsetting flag.");
        }
        dstAdjusted = false;
    }        
}

std::vector<std::string> split(std::string s, std::string delim) {
    size_t posStart = 0;
    size_t posEnd = 0;
    size_t delimLen = delim.length();
    std::string token;
    std::vector<std::string> result;
    
    while((posEnd = s.find(delim, posStart)) != std::string::npos) {
        token = s.substr(posStart, posEnd - posStart);
        posStart = posEnd + delimLen;
        result.push_back(token);
    }

    result.push_back(s.substr(posStart));
    return result;
}

void executeCmd(void) {
    std::vector<std::string> parts = split(cmd, " ");
    cmd.clear();
    if(parts.size() <= 0) {
        Serial.println("Invalid command.");
        return;
    }

    if(parts[0] == "time") {
        if(parts.size() < 2) {
            Serial.println("Malformed time command. No time supplied.");
            return;
        }

        std::vector<std::string> timeParts = split(parts[1], ":");
        if(timeParts.size() != 3) {
            Serial.println("Malformed time command. Invalid time supplied.");
            return;
        }

        String h(timeParts[0].c_str());
        String m(timeParts[1].c_str());
        String s(timeParts[2].c_str());

        uint8_t hour = 0;
        uint8_t minutes = 0;
        uint8_t seconds = 0;

        // NOTE: If we asked for 0 then make it zero. If we didn't ask for 0 and we get 0 from the conversion
        // NOTE: then there has been a problem.
        if(h == "0" || h == "00") {
            hour = 0;
        } else {
            hour = h.toInt();
            if((hour <= 0) || (hour > 23)) {
                Serial.println("Malformed time command. Invalid hour.");
                return;
            }
        }

        if(m == "0" || m == "00") {
            minutes = 0;
        } else {
            minutes = m.toInt();
            if((minutes <= 0) || (minutes > 59)) {
                Serial.println("Malformed time command. Invalid minutes.");
                return;
            }
        }

        if(s == "0" || s == "00") {
            seconds = 0;
        } else {
            seconds=  s.toInt();
            if((seconds <= 0) || (seconds > 59)) {
                Serial.println("Malformed time command. Invalid seconds.");
                return;
            }
        }

        nextion.writeNum("sleep", 0);
        nextion.writeNum("rtc3", hour);
        nextion.writeNum("rtc4", minutes);
        nextion.writeNum("rtc5", seconds);
        rtc.setHours(hour);
        rtc.setMinutes(minutes);
        rtc.setSeconds(seconds);
    } else if(parts[0] == "date") {
        if(parts.size() < 2) {
            Serial.println("Malformed date command. No date supplied.");
            return;
        }

        std::vector<std::string> dateParts = split(parts[1], "/");
        if(dateParts.size() != 3) {
            Serial.println("Malformed date command. Invalid date supplied.");
            return;
        }

        String d(dateParts[0].c_str());
        String m(dateParts[1].c_str());
        String y(dateParts[2].c_str());

        uint8_t days = 0;
        uint8_t months = 0;
        int year = 0;

        days = d.toInt();
        if((days <= 0) || (days > 31)) {
            Serial.println("Malformed date command. Invalid days.");
            return;
        }

        months = m.toInt();
        if((months <= 0) || (months > 12)) {
            Serial.println("Malformed date command. Invalid months.");
            return;
        }

        year = y.toInt();
        if((year < 2000) || (year > 2099)) {
            Serial.println("Malformed date command. Invalid year.");
            return;
        }

        nextion.writeNum("sleep", 0);
        nextion.writeNum("rtc2", days);
        nextion.writeNum("rtc1", months);
        nextion.writeNum("rtc0", year);
        rtc.setDay(days);
        rtc.setMonth(months);
        rtc.setYear(year - 2000);
    } else if(parts[0] == "screenreset") {
        nextion.writeStr("rest");
    } else if(parts[0] == "reset") {
        // NOTE: A six second delay will causea a watchdog reset.
        delay(6000);
    } else if(parts[0] == "wakeupscreen") {
        Serial.println("Waking screen.");
        nextion.writeNum("sleep", 0);
    } else if(parts[0] == "sleepscreen") {
        Serial.println("Waking screen.");
        nextion.writeNum("sleep", 1);
    } else if(parts[0] == "legsdata") {
        cmdMode = false;
        showLegsData = !showLegsData;
#ifdef TESTING
#endif
    } else if(parts[0] == "exit") {
        cmdMode = false;
    } else {
        Serial.print("Unknown command: ");
        Serial.println(parts[0].c_str());
    }
}

void showCmdMenu(void) {
#if 0
    sprintf(outputBuf, "%c[2J%c[H", 0x1B, 0x1B);
    Serial.print(outputBuf);

    sprintf(outputBuf, "Current: %.3f\n\rSoC: %d", 
            screenData.current, screenData.soc);
    Serial.println(outputBuf);

    sprintf(outputBuf, "\n\rScreen page: %d\n\rScreen is asleep: %s\n\r", 
            currentPage, flags.screenIsAsleep ? "YES" : "NO");
    Serial.println(outputBuf);

    Serial.println("Flags:");
    sprintf(outputBuf, "Overload:    %s", flags.overload ? "ON" : "OFF");
    Serial.println(outputBuf);
    sprintf(outputBuf, "Overvotlage: %s", flags.overVoltage ? "ON" : "OFF");
    Serial.println(outputBuf);
    sprintf(outputBuf, "Overheat:    %s", flags.batteryOverheat ? "ON" : "OFF");
    Serial.println(outputBuf);
    sprintf(outputBuf, "CooledOff:   %s", flags.cooledOff ? "ON" : "OFF");
    Serial.println(outputBuf);
    sprintf(outputBuf, "Moving:      %s", flags.moving ? "ON" : "OFF");
    Serial.println(outputBuf);

    sprintf(outputBuf, "\n\rEnter command: %s", cmd.c_str());
    Serial.print(outputBuf);
#else
    Serial.printf("%c[2J%c[H", 0x1B, 0x1B);
    Serial.printf("Current: %.3f\n\rSoC: %d\n\r", screenData.current, screenData.soc);
    Serial.printf("\n\rScreen page: %d\n\rScreen is asleep: %s\n\n\r", currentPage, flags.screenIsAsleep ? "YES" : "NO");

    Serial.println("Flags:");
    Serial.printf("Overload:    %s\n\r", flags.overload ? "ON" : "OFF");
    Serial.printf("Overvotlage: %s\n\r", flags.overVoltage ? "ON" : "OFF");
    Serial.printf("Overheat:    %s\n\r", flags.batteryOverheat ? "ON" : "OFF");
    Serial.printf("CooledOff:   %s\n\r", flags.cooledOff ? "ON" : "OFF");
    Serial.printf("Moving:      %s\n\r", flags.moving ? "ON" : "OFF");
    Serial.printf("Load Check:  %s, timer: %d\n\r", flags.moving ? "ON" : "OFF", loadCheckTimer);

    Serial.printf("\n\rEnter command: %s\n\r", cmd.c_str());
#endif
}

void handleSerialInput(const char in) {
    if(cmdMode) {
        cmdModeTimeout = 0;
        if(in == 0x0D) {
            Serial.println();
            executeCmd();
        } else if(in == 0x1B) {
            cmd.clear();
        } else if(in == 0x08 || in == 0x7F) {
            if(cmd.length() > 0) {
                cmd.pop_back();
            }
        } else {
            cmd += in;
        }
    } else {
        switch(in) {
            case 'a': {
                Serial.println("LEG UP.");
                PORT->Group[0].OUT.reg ^= PORT_PA07;
            } break;

            case 'b': { 
                Serial.println("LEG DOWN.");
                PORT->Group[0].OUT.reg ^= PORT_PA11;
            } break;

            case 'c': { 
                Serial.println("LEGS OFF.");
                PORT->Group[0].OUT.reg |= PORT_PA07;
                PORT->Group[0].OUT.reg |= PORT_PA11;
            } break;

            case 'd': { 
                Serial.println("LEG RESET.");
                legs.reset();
            } break;

            case 'e': { 
                Serial.println("LEG CALIBRATE.");
                legs.calibrate();
            } break;

            case 'f': { } break; 
            case 'g': { } break;
            case 'h': { } break; 
            case 'i': { } break;
            case 'j': { } break;
            case 'k': { } break;
            case 'l': { } break;
            case 'm': { } break;

            case 'v': {
                Serial.print("C++ std in use: ");
                Serial.println(__cplusplus);
            } break;

            case 'x': {
                Serial.print("Enter command: ");
                showLegsData = false;
                cmdMode = true;
            } break;

        }
    }
}
