#include "Globals.h"

RTCZero rtc;

Leg legs(TL6_DEFAULT_BASE, TL6_DEFAULT_THROW, TL6_DEFAULT_TOTAL_PULSES, TL6_DEFAULT_MAX_HEIGHT_MM, TL6_DEFAULT_MIN_HEIGHT_MM);
std::list<hmiControllerMessage> incomingQueue;
std::list<hmiControllerMessageCodes_e> outgoingQueue;

// NextionPages_e currentPage = PAGE_MAIN;
int currentPage = PAGE_MAIN;
EasyNex nextion(Serial1);

std::vector<uint8_t> i2cRecvBuf = {};

Flags_t flags = {};
ScreenData_t screenData = {};

long runtimeAccumulator = 0;
uint8_t runtimeAvgCount = 0;
std::string runtime = "";

uint8_t led1CurrentBrightness;
uint8_t led2CurrentBrightness;

std::string extraMsg = "";
std::string cmd = "";
bool cmdMode = false;
bool showLegsData = false;
uint8_t cmdModeTimeout = 0;

uint8_t loadCheckTimer = LOAD_CHECK_TIMEOUT;
