#ifndef TIMERS_H
#define TIMERS_H

#include "Globals.h"

#include <Arduino.h>

void TC3_Init(void);
void TC4_Init(void);
void TC5_Init(void);

#endif /* !TIMERS_H */
