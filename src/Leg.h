
/* *****************************************************************************************************************
 * NOTE: All data is still based on the 24v leg. Even though this is for the 12v leg. I don't have any information 
 * NOTE: for the 12v leg.
 * *****************************************************************************************************************/

/* NOTE: On Leg Size and Measurements. FOR THE 24V LEG!
 *                  Datasheet       My Measurements
 * Retracted           590mm            590mm
 * Thick section       560mm            560mm
 * Stroke              650mm            710mm (680 with retracted)
 * Total length        1240mm           1275mm
 * Pulses UP           1261             ~1920
 * Pulses DOWN         1261             ~1920
 * Pulses per mm       1.94             2.7042 (2.8235 with retracted)
 */

#ifndef LEG_H
#define LEG_H

#include <string>

#include <Arduino.h>
#undef max
#undef min
#undef round
#undef abs

#define FATLEG

//#define TL6_STAND_HEIGHT_MM     1170     // NOTE: This is the actual ergonimic height. But it's higher than max so...

// NOTE: Common limits for all three legs.
#define TL6_SIT_HEIGHT_MM               710
#define TL6_STAND_HEIGHT_MM             990
#define TL6_TIMER_INTERVAL              1       // NOTE: Milliseconds.
#define TL6_GRAY_MATCH_TIMEOUT          0.5     // NOTE: Seconds.
#define TL6_MAX_RUNTIME                 30      // NOTE: Seconds.
#define TL6_END_STOP_COOLDOWN           5       // NOTE: Seconds.

#ifdef SLIM12
    #define TL6_MAX_CURRENT             7.8     // NOTE: From the side of the 12v leg
    #define TL6_DEFAULT_BASE            590     // NOTE: MM.
    #define TL6_DEFAULT_THROW           685     // NOTE: MM.
    #define TL6_DEFAULT_ABS_MAX_HEIGHT_MM 1275  // NOTE: mm. Absolute max height of the 12v & 24v slim legs.
    #define TL6_DEFAULT_TOTAL_PULSES    2055    // NOTE: This is for the 12v and is not from any datasheet. I just counted them.
    #define TL6_DEFAULT_MAX_HEIGHT_MM   990     // NOTE: mm. From Carl
    #define TL6_DEFAULT_MIN_HEIGHT_MM   610     // NOTE: mm. To stop it hitting the bottom when it goes down.
#elif defined SLIM24
    #define TL6_MAX_CURRENT             7.8     // NOTE: From the side of the 12v leg
    #define TL6_DEFAULT_BASE            590     // NOTE: MM.
    #define TL6_DEFAULT_THROW           685     // NOTE: MM.
    #define TL6_DEFAULT_ABS_MAX_HEIGHT_MM 1275  // NOTE: mm. Absolute max height of the 12v & 24v slim legs.
    #define TL6_DEFAULT_TOTAL_PULSES    2650    // NOTE: Pulses. = ~ 1261 (+60) * 2
    #define TL6_DEFAULT_MAX_HEIGHT_MM   990     // NOTE: mm. From Carl
    #define TL6_DEFAULT_MIN_HEIGHT_MM   610     // NOTE: mm. To stop it hitting the bottom when it goes down.
#elif defined FATLEG
    #define TL6_MAX_CURRENT             10      // NOTE: From Alex at TiMotion
    #define TL6_DEFAULT_BASE            700     // NOTE: MM.
    #define TL6_DEFAULT_THROW           400     // NOTE: MM.
    #define TL6_DEFAULT_ABS_MAX_HEIGHT_MM 1100  // NOTE: mm. Absolute max height of the 12v & 24v slim legs.
    #define TL6_DEFAULT_TOTAL_PULSES    2528    // NOTE: This is for the Fat 12v and is not from any datasheet. I just counted them.
    #define TL6_DEFAULT_MAX_HEIGHT_MM   990     // NOTE: mm. From Carl
    #define TL6_DEFAULT_MIN_HEIGHT_MM   700     // NOTE: mm. This is actually the base size on this leg.
#endif

class Leg {
    private:
        struct Status_t {
            bool upOn;
            bool downOn;
            bool goingUp;
            bool goingDown;
            bool atTheTop;
            bool atTheBottom;
            bool coolDown;
            bool grayMatchCutoff;
            bool overCurrentCutoff;
            bool runtimeCutoff;
            bool autoMode;
            bool calibrating;
        };
        
        uint8_t mGrayCode;
        uint32_t mCoolDownTimeout;      // NOTE: Both of these are converted the same way as the gray code match.
        uint32_t mCoolDownCounter;      // NOTE: I made them 32 bit to allow for much longer time outs.
        float mBatteryCurrentAtStart;
        float mLegCurrentUsage;
        uint16_t mGrayCodeMatchTimer;
        uint16_t mGrayCodeMatchValue;
        uint16_t mRuntimeTimeout;
        uint16_t mRuntimeCounter;
        uint16_t mUpPulses;
        uint16_t mDownPulses;
        int16_t mHeightPulses;
        uint16_t mHeightMM;
        uint16_t mTotalPulses;
        float   mPulsePerMM;
        uint16_t mTotalHeightMM;
        uint16_t mBaseHeightMM;
        uint16_t mThrowMM;
        uint16_t mDesiredHeightPulses;
        uint16_t mMaxHeightPulses;
        uint16_t mMaxHeightMM;
        uint16_t mMinHeightPulses;
        uint16_t mMinHeightMM;


        Status_t mStatus;
        uint8_t makeGrayCode(void);
        void startCooldown(const uint32_t timer);
        void cutoffCheck(void);

    public:
        // Leg(void) : mBaseHeightMM(TL6_DEFAULT_BASE), mThrowMM(TL6_DEFAULT_THROW), mTotalPulses(TL6_DEFAULT_TOTAL_PULSES), 
        //             mMaxHeightMM(TL6_DEFAULT_MAX_HEIGHT_MM) {}
        Leg(uint16_t baseHeightMM, uint16_t throwMM, uint16_t totalPulses, uint16_t maxHeightMM, uint16_t minHeightMM);

        Status_t status(void) const { return mStatus; }
        void service(void);
        void up(void);
        void down(void);
        void upOff(void);
        void downOff(void);
        void stop(void);
        void goToHeightPulses(const uint16_t height);
        void goToHeightMM(uint16_t height);
        void calibrate(void);
        void outputDiagnosticInfo(void);
        void outputDiagnosticInfo(const std::string& msg);
        void reset(void);
};

#endif /* !LEG_H */
