#include "Timers.h"

// NOTE: APB = Advanced Peripheral Bus.
// NOTE: AHB = Advanced High Performance Bus.

uint8_t ledBrighness = 0;
bool ledIncrease = true;

void TC3_Init(void) {
    // NOTE: Make sure clock divider for the genertor is 1.
    GCLK->GENDIV.reg = GCLK_GENDIV_ID(4) | GCLK_GENDIV_DIV(1);

    // NOTE: Configure generic clock generator 2 to use the 48Mhz clock as a source and enable it.
    GCLK->GENCTRL.reg = GCLK_GENCTRL_ID(4) | GCLK_GENCTRL_SRC(GCLK_SOURCE_DFLL48M) | GCLK_GENCTRL_GENEN;

    // NOTE: Wait for the clock to be setup.
    while(GCLK->STATUS.reg & GCLK_STATUS_SYNCBUSY);

    // NOTE: Enable generic clock generator 0 and set it as the source for timer TC3.
    GCLK->CLKCTRL.reg = GCLK_CLKCTRL_CLKEN | GCLK_CLKCTRL_GEN(4) | GCLK_CLKCTRL_ID(TC3_GCLK_ID);
    
    // Enable the bus clock for TC3
    PM->APBCMASK.reg |= PM_APBCMASK_TC3;

    // Set the timer to 16 bit count mode and sent match frequency mode (compare match). Set the pre-scaler
    // to 1024.
    TC3->COUNT16.CTRLA.reg |= (TC_CTRLA_MODE_COUNT16 | TC_CTRLA_WAVEGEN_MFRQ | TC_CTRLA_PRESCALER_DIV1024);

    // Set the compare value for one second @ 48Mhz with 1024 prescaler.
    TC3->COUNT16.CC[0].reg = 46874;

    // Enable the compare mode interrupt for channel 0.
    TC3->COUNT16.INTENSET.reg |= TC_INTENSET_MC0;

    // Enable the timer now that it is configured.
    TC3->COUNT16.CTRLA.reg |= TC_CTRLA_ENABLE;

    // Wait for the timer to be enabled.
    while(TC3->COUNT16.STATUS.reg & TC_STATUS_SYNCBUSY);
    
    // Set the priority of the interrupt. Note the lower the number the higher the priority, 0-4.
    NVIC_SetPriority(TC3_IRQn, 2);

    // Enable the interrupt.
    NVIC_EnableIRQ(TC3_IRQn);
}

void TC4_Init(void) {
    
    // NOTE: Set up another clock generator to run another timer.
    GCLK->GENDIV.reg = GCLK_GENDIV_ID(5) | GCLK_GENDIV_DIV(1);
    GCLK->GENCTRL.reg = GCLK_GENCTRL_ID(5) | GCLK_GENCTRL_SRC(GCLK_SOURCE_DFLL48M) | GCLK_GENCTRL_GENEN;
    while(GCLK->STATUS.reg & GCLK_STATUS_SYNCBUSY);
    GCLK->CLKCTRL.reg = GCLK_CLKCTRL_CLKEN | GCLK_CLKCTRL_GEN(5) | GCLK_CLKCTRL_ID(TC4_GCLK_ID);

    PM->APBCMASK.reg |= PM_APBCMASK_TC4;
    TC4->COUNT16.CTRLA.reg |= (TC_CTRLA_MODE_COUNT16 | TC_CTRLA_WAVEGEN_MFRQ | TC_CTRLA_PRESCALER_DIV256);
    // TC4->COUNT16.CC[0].reg = 46874; // NOTE: 1/4 of a second (250 ms) (prescaler is differnt from TC3)
    // TC4->COUNT16.CC[0].reg = 1875; // NOTE: 10ms
    TC4->COUNT16.CC[0].reg = 187; // NOTE: 1ms
    TC4->COUNT16.INTENSET.reg |= TC_INTENSET_MC0;
    TC4->COUNT16.CTRLA.reg |= TC_CTRLA_ENABLE;
    while(TC4->COUNT16.STATUS.reg & TC_STATUS_SYNCBUSY);
    NVIC_SetPriority(TC4_IRQn, 2);
    NVIC_EnableIRQ(TC4_IRQn);
}

void TC5_Init(void) {
    // NOTE: TC4 and 5 share a clock generator.
    
    PM->APBCMASK.reg |= PM_APBCMASK_TC5;
    TC5->COUNT16.CTRLA.reg |= (TC_CTRLA_MODE_COUNT16 | TC_CTRLA_WAVEGEN_MFRQ | TC_CTRLA_PRESCALER_DIV1024);
    TC5->COUNT16.CC[0].reg = 23536; // NOTE: 1/2 second.
    TC5->COUNT16.INTENSET.reg |= TC_INTENSET_MC0;
    TC5->COUNT16.CTRLA.reg |= TC_CTRLA_ENABLE;
    while(TC5->COUNT16.STATUS.reg & TC_STATUS_SYNCBUSY);
    NVIC_SetPriority(TC5_IRQn, 2);
    NVIC_EnableIRQ(TC5_IRQn);
}

void TC3_Handler(void) {
    // NOTE: The act of reading the register clears the bit.
    if(TC3->COUNT16.INTFLAG.reg & TC_INTFLAG_MC0) {
        // Re-set the interrupt (setting the bit is the reset).
        TC3->COUNT16.INTFLAG.reg |= TC_INTFLAG_MC0;

        flags.checkScreenSleep = 1;
        if(cmdMode) {
            ++cmdModeTimeout;
        }

        if(flags.loadCheck) {
            ++loadCheckTimer;
        }
    }
}

void TC4_Handler(void) {
    if(TC4->COUNT16.INTFLAG.reg & TC_INTFLAG_MC0) {
        TC4->COUNT16.INTFLAG.reg |= TC_INTFLAG_MC0;
        legs.service();
    }
}

void TC5_Handler(void) {
    if(TC5->COUNT16.INTFLAG.reg & TC_INTFLAG_MC0) {
        TC5->COUNT16.INTFLAG.reg |= TC_INTFLAG_MC0;
        flags.updateScreen = 1;
    }
}
