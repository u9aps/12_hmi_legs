CLI = arduino-cli
FQBN = Seeeduino:samd:seeed_XIAO_m0
CORE = Seeeduino:samd
BOARD = Seeeduino XIAO
# SERIAL = $(shell $(CLI) board list | grep "Arduino" | awk '{ print $$1; }')

SKETCH = hmilegs12v

.PHONY  = clean serial docs

all:
	@echo "Compiling $(SKETCH)"
	@$(CLI) compile -o ./bin/$(SKETCH) --fqbn $(FQBN) ./src/hmilegs12v.ino

upload0:
	@echo "Uploading $(SKETCH)"
	@$(CLI) upload -i ./bin/$(SKETCH) -p /dev/ttyACM0 --fqbn $(FQBN) --verify

upload1:
	@echo "Uploading $(SKETCH)"
	@$(CLI) upload -i ./bin/$(SKETCH) -p /dev/ttyACM1 --fqbn $(FQBN) --verify

clean:
	@echo "Cleaning up."
	@rm ./bin/*.elf
	@rm ./bin/*.bin

serial0:
	@screen /dev/ttyACM0 115200 8n1

serial1:
	@screen /dev/ttyACM1 115200 8n1

docs:
	@echo "Making docs."
	@doxygen Doxyfile

